from flask import Flask, request, jsonify, make_response
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash
import uuid
import jwt
import datetime
import random
from functools import wraps

app = Flask(__name__)

app.config['SECRET_KEY']='004f2af45d3a4e161a7dd2d17fdae47f'
app.config['SQLALCHEMY_DATABASE_URI']='sqlite:///carstore.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True

db = SQLAlchemy(app)

class Users(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    public_id = db.Column(db.Integer)
    name = db.Column(db.String(50), unique=True, nullable=False)
    password = db.Column(db.String(50), nullable=False)
    admin = db.Column(db.Boolean)

class Cars(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.id'), nullable=False)
    car_id = db.Column(db.String, nullable=False)
    name = db.Column(db.String(50), nullable=False)
    company = db.Column(db.String(50), nullable=True)
    model = db.Column(db.String(50), nullable=True)

#Create DB
with app.app_context():
    db.create_all()


#Function wrapper acting as API Gateway
def token_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):

        token = None

        if 'jwt-access-token' in request.headers:
            token = request.headers['jwt-access-token']

        if not token:
            return jsonify({'message': 'a valid token is missing'})
        
        try:
            data = jwt.decode(token, app.config['SECRET_KEY'], algorithms=["HS256"])
            current_user = Users.query.filter_by(public_id=data['public_id']).first()
        except:
            return jsonify({'message': 'token is invalid'})

        return f(current_user, *args, **kwargs)
    return decorator


#Register endpoint which lets user register via username and password
@app.route('/register', methods=['POST'])
def signup_user():  
    data = request.get_json()  

    hashed_password = generate_password_hash(data['clientPassword'], method='sha256')
 
    new_user = Users(public_id=str(uuid.uuid4()), name=data['clientID'], password=hashed_password, admin=False)
    try:
        db.session.add(new_user)
        db.session.commit()
    except:
        return jsonify({'message' : 'Username already exists. Please choose another username'})

    return jsonify({'message': 'Registration successful'})

#Login endpoint which lets user obtain access token for API access
@app.route('/login', methods=['POST'])  
def login_user(): 
    auth = request.authorization   

    if not auth or not auth.username or not auth.password:  
        return make_response('could not verify', 401, {'Authentication': 'login required"'})    

    user = Users.query.filter_by(name=auth.username).first()
     
    if check_password_hash(user.password, auth.password):

        token = jwt.encode({'public_id' : user.public_id, 'exp' : datetime.datetime.utcnow() + datetime.timedelta(minutes=45)}, app.config['SECRET_KEY'], "HS256")
        return jsonify({'token' : token}) 

    return make_response('could not verify',  401, {'Authentication': '"login required"'})

#Endpoint to get all registered users
@app.route('/api/users', methods=['GET'])
def get_all_users():  
   
    users = Users.query.all() 
    result = []   
    for user in users:   
        user_data = {}   
        user_data['public_id'] = user.public_id  
        user_data['name'] = user.name 
        user_data['password'] = user.password
        user_data['admin'] = user.admin 
       
        result.append(user_data)   

    return jsonify({'users': result})

#Endpoint to verify if car is registered or not
@app.route('/api/hwpa', methods=['POST'])
@token_required
def hwpa(current_user):
   
    data = request.get_json() 
    #car = Cars.query.filter_by(car_id=data['carID'], user_id=current_user.id).first()
    car = Cars.query.filter_by(car_id=data['carID']).first()
    if not car:   
        return jsonify({'registered': 'False'})

    return jsonify({'registered': 'True'})

#Endpoint to create new car
@app.route('/api/car', methods=['POST'])
@token_required
def create_car(current_user):
   
    data = request.get_json() 

    new_cars = Cars(car_id=data['carID'], name=data['name'], company=data['company'], user_id=current_user.id, model=data['model'])
    db.session.add(new_cars)
    db.session.commit()   

    return jsonify({'message' : 'new car created'})

#Fetch all cars that are registered
@app.route('/api/cars', methods=['GET'])
@token_required
def get_cars(current_user):

    #cars = Cars.query.filter_by(user_id=current_user.id).all()
    cars = Cars.query.all()

    output = []
    for car in cars:
        car_data = {}
        car_data['id'] = car.id
        car_data['name'] = car.name
        car_data['car_id'] = car.car_id
        car_data['company'] = car.company
        car_data['model'] = car.model
        
        output.append(car_data)

    return jsonify({'List of Cars' : output})


if  __name__ == '__main__':  
     app.run(debug=True)