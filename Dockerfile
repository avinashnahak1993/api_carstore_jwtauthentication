FROM python:3.9

#Set working directory
WORKDIR /app

#Copy contents to working directory
COPY . /app

#Install requirements
RUN pip install -r requirements.txt

#Expose port
EXPOSE 5000

#Run Flask app
#CMD ["python", "app.py"]

CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0"]