Docker Build Command
 docker build -t carstore .

Docker Run
 docker run -it -p 5000:5000 carstore

Access Token Name : jwt-access-token (To be set in the header when making requests to API and Services)

Endpoints with Input and Output Description
1. /Register
Performs the operation of adding users 
Input Type : raw JSON in Body
{
    "clientID": "Sacha",
    "clientPassword": "123"
}

Output Type : JSON
{
    "message": "Registration successful"
}

2. /api/users
Gets the list of registered users
{
    "users": [
        {
            "admin": false,
            "name": "avinash",
            "password": "sha256$OTMVahzCW6qI6fu8$ae1e75e0ec3f46dbf4839cb6a5f7727716628c5572648e41bbb375b2a2ca7535",
            "public_id": "3d6ae780-d2db-48ce-9ce5-aa1d95ea2864"
        },
        {
            "admin": false,
            "name": "bonny",
            "password": "sha256$0kbaN6xV8XYX3Tt3$8d495c0c8ba3e35231a8399d3c7caeb5812712c3c368082916641f2da136fd99",
            "public_id": "27f51f94-f095-4f6a-989f-6d30019daf6b"
        },
        {
            "admin": false,
            "name": "sourabh",
            "password": "sha256$IURw7wowYyjmeJST$b5c57d167bfdfdf40f370cbca94405ec847ed86e30e1b2d71082a970aa970b6c",
            "public_id": "8f5a3d09-3e11-4787-a645-c4dc0d4a90d5"
        },
        {
            "admin": false,
            "name": "Sacha",
            "password": "sha256$OkinlpUohg9EDdqQ$8267a26564de5e3f189b8ea30ad756d4b1799d89a33f5ab4ab09c26dcd81dff7",
            "public_id": "2d49e750-9d7c-48b2-8235-2f38a6d62127"
        }
    ]
}

3. /login
Input Type : Authorization
Username : Avinash (corresponds to clientID)
Password : 123 (corresponds to clientPassword)
Output Type : JSON
{
    "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJwdWJsaWNfaWQiOiI4ZjVhM2QwOS0zZTExLTQ3ODctYTY0NS1jNGRjMGQ0YTkwZDUiLCJleHAiOjE2OTEzNDE3NTJ9.aPCs6Ny7C1N5C7X1bSR3_Qfw9X4HzJfk7eoC9_w7eQ4"
}

4. /api/car
Input Type : JSON
Output Type : JSON
Required Header: jwt-access-token
Example:
{
    "carID": "C1",
    "name": "Personal Car",
    "company": "Nissan",
    "model": "Sunny"
}

{
    "message": "new car created"
}

5. /api/cars
Gets the list of cars
Required Header: jwt-access-token
Output Type : JSON
Output Example:
{
    "List of Cars": [
        {
            "car_id": "A1",
            "company": "Renault",
            "id": 1,
            "model": "Scala",
            "name": "Personal Car"
        },
        {
            "car_id": "B1",
            "company": "Renault",
            "id": 2,
            "model": "Duster",
            "name": "Personal Car"
        },
        {
            "car_id": "C1",
            "company": "Nissan",
            "id": 3,
            "model": "Sunny",
            "name": "Personal Car"
        }
    ]
}

6. /api/hwpa
Required Header: jwt-access-token
Input Type : JSON
Output Type : JSON

Example:
Input:
{
    "carID":"C1"
}	
Output:
{
    "registered": "True"
}
Input:
{
    "carID":"C2"
}
Output:
{
    "registered": "False"
}